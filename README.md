# Déploiement d'un notebook

> Mettre à disposition sur internet un notebook Jupyter

## Mise en place

1. Installer les dépendances :

```bash
pipenv install -d
pipenv shell
```

2. Lancer Jupyter Notebook pour éditer la présentation :

```bash
jupyter notebook
```

3. Lancer le serveur web local pour lancer la présentation :

```bash
jupyter nbconvert --to=slides --post=serve presentation.ipynb
```

4. Générer la présentation au format HTML :

```bash
jupyter nbconvert --to=slides --output-dir=dist --output=index --SlidesExporter.file_extension=.html presentation.ipynb
cp -R static dist
```
